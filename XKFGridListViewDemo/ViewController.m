//
//  ViewController.m
//  XKFGridListViewDemo
//
//  Created by Audient Xie on 2018/3/6.
//  Copyright © 2018年 abyss. All rights reserved.
//

#import "ViewController.h"
#import "XKFGridListView.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet XKFGridListView *gridView;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    NSArray *arr = @[@"tttt1",@"tttt222",@"tttt333", @"tttt4444",@"tttt55"];
    [self.gridView setupData:arr countPerLine:3];
    for ( UIButton *btn in self.gridView.gridItemViewList ) {
        [btn addTarget:self action:@selector(gridTap:) forControlEvents:(UIControlEventTouchUpInside)];
    }
    [self.gridView.gridItemViewList.firstObject setSelected:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)gridTap:(id)sender {
    [self.gridView setGridSelected:sender];
}


@end
