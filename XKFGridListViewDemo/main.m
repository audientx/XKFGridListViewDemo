//
//  main.m
//  XKFGridListViewDemo
//
//  Created by Audient Xie on 2018/3/6.
//  Copyright © 2018年 abyss. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
