//
//  XKFGridListView.m
//  WL2017Clean
//
//  Created by Audient Xie on 2018/3/6.
//  Copyright © 2018年 abyss. All rights reserved.
//

#import "XKFGridListView.h"

@implementation XKFGridListView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/



- (void) setupData:(NSArray*)datas countPerLine:(NSInteger)count {
    NSAssert(count > 0,@"XKFGridListView countPerLine <= 0 !!! ");
    
    self.translatesAutoresizingMaskIntoConstraints = NO;
    
    if ( self.gridItemViewList.count > 0 || datas.count == 0 ) {
        // 不重复设置
        return;
    }
    
    CGFloat width = self.frame.size.width / count;
    NSInteger line = ( datas.count + count - 1 ) / count;
    
    for ( NSInteger i = 0; i<line; i++ ) {
        for( NSInteger j = 0; j<count && j<(datas.count-i*count) ; j++) {
            
            UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
            NSInteger index = i * count + j;
            btn.titleLabel.font = [UIFont systemFontOfSize:14];
            [btn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
            [btn setTitleColor:[UIColor orangeColor] forState:UIControlStateSelected];
            [btn setTitle:datas[index] forState:UIControlStateNormal];
            CGRect rect =  {j*width, i*XKFGrid_Height, width, XKFGrid_Height};
            btn.frame = rect;
            btn.tag = index;
            [self.gridItemViewList addObject:btn];

            [self addSubview:btn];
            btn.translatesAutoresizingMaskIntoConstraints = NO;

        }
    }
    
    
    UIButton *btn1 = self.gridItemViewList.firstObject;
    NSLayoutConstraint *constraintWidth = [NSLayoutConstraint constraintWithItem:btn1
                                                                        attribute:NSLayoutAttributeWidth
                                                                        relatedBy:NSLayoutRelationEqual
                                                                           toItem:self
                                                                        attribute:NSLayoutAttributeWidth
                                                                       multiplier:(1.f/count)
                                                                         constant:0];
    [self addConstraint:constraintWidth];
    
    for ( NSInteger i = 0; i<line; i++ ) {
        for( NSInteger j = 0; j<count && j<(datas.count-i*count) ; j++) {
            NSInteger index = i * count + j;
            UIButton *btn = self.gridItemViewList[index];
            
            NSLayoutConstraint *constraintHeight = [NSLayoutConstraint constraintWithItem:btn
                                                                                attribute:NSLayoutAttributeHeight
                                                                                relatedBy:NSLayoutRelationEqual
                                                                                   toItem:nil
                                                                                attribute:NSLayoutAttributeNotAnAttribute
                                                                               multiplier:1
                                                                                 constant:XKFGrid_Height];
            [btn addConstraint:constraintHeight];
            
            if ( index > 0 ) {
                NSLayoutConstraint *constraintWidth = [NSLayoutConstraint constraintWithItem:btn
                                                                                    attribute:NSLayoutAttributeWidth
                                                                                    relatedBy:NSLayoutRelationEqual
                                                                                       toItem:btn1
                                                                                    attribute:NSLayoutAttributeWidth
                                                                                   multiplier:1
                                                                                    constant:0];
                [self addConstraint:constraintWidth];
            }
            
            if ( j == 0 ) {
                NSLayoutConstraint *constraintLeft = [NSLayoutConstraint constraintWithItem:btn
                                                                                  attribute:NSLayoutAttributeLeading
                                                                                  relatedBy:NSLayoutRelationEqual
                                                                                     toItem:self
                                                                                  attribute:NSLayoutAttributeLeading
                                                                                 multiplier:1
                                                                                   constant:0];
                [self addConstraint:constraintLeft];
            }
            else {
                NSLayoutConstraint *constraintLeft = [NSLayoutConstraint constraintWithItem:btn
                                                                                  attribute:NSLayoutAttributeLeading
                                                                                  relatedBy:NSLayoutRelationEqual
                                                                                     toItem:self.gridItemViewList[index-1]
                                                                                  attribute:NSLayoutAttributeTrailing
                                                                                 multiplier:1
                                                                                   constant:0];
                [self addConstraint:constraintLeft];
                
            }
            
            
            if ( i == 0) {
                
                NSLayoutConstraint *constraintTop = [NSLayoutConstraint constraintWithItem:btn
                                                                                 attribute:NSLayoutAttributeTop
                                                                                  relatedBy:NSLayoutRelationEqual
                                                                                     toItem:self
                                                                                  attribute:NSLayoutAttributeTop
                                                                                 multiplier:1
                                                                                   constant:0];
                [self addConstraint:constraintTop];
                
                
            }
            else {
                
                NSLayoutConstraint *constraintTop = [NSLayoutConstraint constraintWithItem:btn
                                                                                 attribute:NSLayoutAttributeTop
                                                                                 relatedBy:NSLayoutRelationEqual
                                                                                    toItem:self.gridItemViewList[index-count]
                                                                                 attribute:NSLayoutAttributeBottom
                                                                                multiplier:1
                                                                                  constant:0];
                [self addConstraint:constraintTop];
                
            }
        }
    }

}


- (void) setGridSelected:(id)btn1 {
    for (UIButton* btn in self.gridItemViewList ) {
        btn.selected = ( btn == btn1);
    }
}


- (NSMutableArray* ) gridItemViewList {
    if ( _gridItemViewList == nil ) {
        _gridItemViewList = [@[] mutableCopy];
    }
    return _gridItemViewList;
}

@end
