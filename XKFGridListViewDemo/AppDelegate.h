//
//  AppDelegate.h
//  XKFGridListViewDemo
//
//  Created by Audient Xie on 2018/3/6.
//  Copyright © 2018年 abyss. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

