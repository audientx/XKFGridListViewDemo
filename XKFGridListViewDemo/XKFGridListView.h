//
//  XKFGridListView.h
//  WL2017Clean
//
//  Created by Audient Xie on 2018/3/6.
//  Copyright © 2018年 abyss. All rights reserved.
//

#import <UIKit/UIKit.h>

#define XKFGrid_Height 30.f
#define XKFGrid_CountPerLine 4



@interface XKFGridListView : UIView

@property (strong, nonatomic) NSMutableArray *gridItemViewList;

- (void) setupData:(NSArray*)datas countPerLine:(NSInteger)count;

- (void) setGridSelected:(id)btn1;

@end
